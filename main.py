#!/usr/bin/env python3
import argparse


def main():
    parser = argparse.ArgumentParser(
        "Generate Club parkrun Competition results HTML from the parkrun website."
    )
    parser.add_argument(
        "-d",
        "--download",
        action="store_true",
        help="Whether to download new data from the website",
    )
    args = parser.parse_args()
    if args.download:
        import Download

        Download.main()
    import GenerateHTML

    GenerateHTML.main()
    GenerateHTML.main()


if __name__ == "__main__":
    main()
