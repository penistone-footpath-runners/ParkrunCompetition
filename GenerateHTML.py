#!/usr/bin/env python3
import os
import random
import collections

with open("parkrunners.csv", "r") as f:
    runner_names = f.read().split("\n")

while "" in runner_names:
    runner_names.remove("")

runner_names = [row.strip(",").split(",")[1:] for row in runner_names]
runner_names = {row[0]: [None] if row[1] == "None" else row[1:] for row in runner_names}


def load_list(filename):
    """Load a newline-separated list from a text file"""
    with open(filename, "r") as f:
        lst = f.read().split("\n")
    while "" in lst:
        lst.remove("")
    return lst


PARKRUNS = load_list("counters.txt")
dates = load_list("dates.txt")

MONTHS = (
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
)

with open("year.txt", "r") as f:
    YEAR = f.read().strip(" \n")
PATHTOHTML = f"parkrun{YEAR}/"

PARKRUNS.append("Wild Card")

POINTS = {
    "Age Grade": {
        125: 6,
        120: 6,
        115: 6,
        110: 6,
        105: 6,
        100: 6,
        95: 6,
        90: 6,
        85: 6,
        80: 6,
        75: 6,
        70: 6,
        65: 5,
        60: 4,
        55: 3,
        50: 2,
        45: 1,
        40: 1,
        35: 1,
        30: 1,
        25: 1,
        20: 1,
        15: 1,
        10: 1,
        5: 1,
        0: 1,
    },
    "Cat Pos": ["", 6, 5, 4, 3, 3, 2, 2, 2, 2, 2] + [1] * 10000,
    "Gender Pos": [0],
    "PB": {1: 3},
}

TOPNUMBER = 5

MINIWIDTH = 512

BESTHIGH = f"Best {TOPNUMBER} age grades <strong>highlighted</strong>"

# Get the HTML headings from their seperate files
with open("heading.html", "r") as f:
    ALLHEAD = f.read()

ALLHEAD = ALLHEAD.replace("{YEAR}", YEAR)

with open("back.html", "r") as f:
    BACKHEAD = ALLHEAD + f.read()

BACKHEAD = BACKHEAD.replace("{MINIWIDTH}", str(MINIWIDTH))

with open("minitable.html", "r") as f:
    MFHEAD = ALLHEAD + f.read()

MFHEAD = MFHEAD.replace("{MINIWIDTH}", str(MINIWIDTH))
MFHEAD = MFHEAD.replace("{DAY}", dates[-1])

VHEADS = [["Name", "Times Volunteered", "Points"]]


class RunnersNotFoundError(BaseException):
    pass


def count_unique(data):
    """Count the unique members of an iterable"""
    return len(set(data))


def write_html(filename, text):
    """Write a string to a .html file in the correct place"""
    with open(f"{PATHTOHTML}{filename}.html", "w") as f:
        f.write(text)


def tourpoints(tourisms):
    return min(tourisms, 20)


def volunteeringpoints(volunteeredtimes):
    return min(volunteeredtimes, 3) * 14 + min(volunteeredtimes, 20) * 1


def create_html_table(data, headingrow=0, columnalign=None, link=True, highlight=False):
    """Creates an HTML table from some data"""
    if columnalign is None:
        columnalign = ["text-align:center"] * len(data[0])
    digitsneeded = []
    for i in range(0, len(data[0]), 1):
        digitsneeded.append(
            max(
                len(str(round(x[i]))) if isinstance(type(x[i]), (float, int)) else 0
                for x in data
            )
        )
    table = "<table>"
    for i, row in enumerate(data):
        table += "<tr>"
        for j, cell in enumerate(row):
            text = str(cell)
            if isinstance(cell, int):
                text = text.rjust(digitsneeded[j], " ")
            if link and os.path.exists(PATHTOHTML + text + ".html"):
                text = '<a href="' + text + '.html">' + text + "</a>"
            if highlight:
                if row in highlight:
                    text = "<strong>" + text + "</strong>"
            if i == headingrow:
                if j == 0:
                    table += (
                        '<th class="sticky" style="text-align:left">' + text + "</th>"
                    )
                else:
                    table += (
                        '<th class="sticky" style="text-align:center">' + text + "</th>"
                    )
            else:
                table += '<td style="' + columnalign[j] + '">' + text + "</td>"

        table += "</tr>"
    table += "</table>"

    return table


def get_points(run, headings):
    """Calculate the points a run has achieved"""
    run = run.copy()
    points = 0
    pointsbd = {}
    ag = int(float(run[headings.index("Age Grade")]))
    ag -= ag % 5
    run[headings.index("Age Grade")] = ag
    for i, field in enumerate(run):
        if headings[i] in POINTS.keys():
            try:
                fieldpoints = POINTS[headings[i]][field]
                points += fieldpoints
                pointsbd[headings[i] + " Points"] = fieldpoints
            except (KeyError, IndexError):
                pointsbd[headings[i] + " Points"] = 0

    return points, pointsbd


def remove_columns(data, columns):
    """Remove the item at columns from each list in the list"""
    newdata = []
    for row in data:
        newrow = []
        for i, cell in enumerate(row):
            if i not in columns:
                newrow.append(cell)
        newdata.append(newrow)
    return newdata


def reorder(headings, newheadings, data):
    """Reorder the table columns"""
    newheadingids = [headings.index(i) for i in newheadings]
    return [[x[y] for y in newheadingids] for x in data]


def find_highest_only(data, unindex, hiindex, n=1, special=None):
    """Returns only the row with the highest (up to nth highest)
    hiindex for all rows with the same unindex"""
    if special is None:
        special = {}
    perfs = {}
    for row in data:
        # No others
        if row[unindex] not in perfs.keys():
            perfs[row[unindex]] = [row]
        # Not enough others
        elif len(perfs[row[unindex]]) < n or (
            row[unindex] in special.keys()
            and len(perfs[row[unindex]]) < special[row[unindex]]
        ):
            perfs[row[unindex]].append(row)
        # Large enough
        elif row[hiindex] > min(x[hiindex] for x in perfs[row[unindex]]):
            worst = None
            pe = 999999
            for perf in perfs[row[unindex]]:
                if perf[hiindex] < pe:
                    worst = perf
                    pe = perf[hiindex]
            perfs[row[unindex]][perfs[row[unindex]].index(worst)] = row

    ones = list(perfs.keys())
    ps = list(perfs.values())
    nps = []
    for p in ps:
        nps += p
    return [ones, nps]


def uk_date(date):
    """Convert from an ISO short date string to a UK written date"""
    parts = date.split("-")
    month = MONTHS[int(parts[1].lstrip("0")) - 1]
    day = parts[2].lstrip("0")
    # Teens are special
    if len(day) >= 2 and day[-2] == "1":
        day += "th"
    elif day[-1] == "1":
        day += "st"
    elif day[-1] == "2":
        day += "nd"
    elif day[-1] == "3":
        day += "rd"
    else:
        day += "th"

    return f"{day} {month}"


def main():
    mtheads = [
        "parkrun",
        "Location",
        "number",
        "Date",
        "parkrunner",
        "Time",
        "Age Cat",
        "Age Grade",
        "Gender",
        "Gender Pos",
        "PB",
        "Cat Pos",
        "Total Points",
        "Age Grade Points",
        "Gender Pos Points",
        "PB Points",
        "Cat Pos Points",
    ]

    newheads = [
        "parkrun",
        "Location",
        "number",
        "parkrunner",
        "Date",
        "Time",
        "Gender",
        "Gender Pos",
        "Gender Pos Points",
        "Age Cat",
        "Cat Pos",
        "Cat Pos Points",
        "Age Grade",
        "Age Grade Points",
        "PB",
        "PB Points",
        "Total Points",
    ]

    mhids = dict(zip(mtheads, range(len(mtheads))))
    nhids = dict(zip(newheads, range(len(mtheads))))

    # These are the text alignments for each column
    newheadsalign = [
        "text-align:left",
        "text-align:left",
        "text-align:left",
        "text-align:left",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
        "text-align:center",
    ]

    fulltablealign = (
        ["text-align:left", "text-align:center", "text-align:center",]
        + ["text-align:center"] * len(PARKRUNS)
        + ["text-align:center", "text-align:center", "text-align:center",]
    )

    headingcount = len(mtheads)
    maintable = []

    if not os.path.exists(PATHTOHTML):
        os.makedirs(PATHTOHTML)

    # Get the data generated by the downloader
    with open("data.txt", "r") as f:
        data = eval(f.read())

    maintable = data[0]
    vpoints = data[1]
    totalparkruns = data[2]

    maintable = [
        [row[0]] + row if row[0] in PARKRUNS else ["Wild Card"] + row
        for row in maintable
    ]

    # Filter out the non-Penistone runners and change their names to
    # the ones in the club database

    # Check names against the table
    errors = []
    nmt = maintable.copy()
    for run in maintable:
        name = run[mhids["parkrunner"]]
        try:
            newname = runner_names[name][0]
        except KeyError:
            errors.append(f'{name} is not found. Ran at {run[mhids["Location"]]}.')
            continue
        # Filter out non-members and runs before the join date
        if newname is None or run[mhids["Date"]] < runner_names[name][1]:
            nmt.remove(run)
            continue
        run[mhids["parkrunner"]] = newname
    if errors:
        print("\n".join(errors))
        raise RunnersNotFoundError("Runners missing from names file.")
    maintable = nmt

    # Make the age categories use n-dashes
    for run in maintable:
        run[mhids["Age Cat"]] = run[mhids["Age Cat"]].replace("-", "–")

    # Calculate points
    for run in maintable:
        tp, fp = get_points(run, mtheads)
        run.append(tp)

        # Make the space for the points
        while len(run) < headingcount:
            run.append(None)

        # Assign points to the spaces
        for k in fp:
            run[mtheads.index(k)] = fp[k]

    # Reorder the table columns
    maintable = reorder(mtheads, newheads, maintable)

    # Make the PB and Age Grade field more readable
    for run in maintable:
        run[nhids["PB"]] = ("", "Yes")[run[nhids["PB"]]]
        run[nhids["Age Grade"]] += "%"

    # Find all of the different names and parkruns
    names = set()
    for run in maintable:
        names.add(run[nhids["parkrunner"]])

    # Create the (invisible) "main.html" file of all runs
    maintext = f"{BACKHEAD}<h2>Master Table – Hidden by Default</h2>{create_html_table([newheads] + maintable)}"
    write_html("main", maintext)

    # Create the per-parkrun files
    for parkrun in PARKRUNS:
        allowed = filter(lambda x: x[nhids["parkrun"]] == parkrun, maintable)
        # 3 Wild Cards allowed
        if parkrun == "Wild Card":
            n = 3
        else:
            n = 1

        allowed = find_highest_only(
            allowed, nhids["parkrunner"], nhids["Total Points"], n=n
        )[1]

        # Sort by name
        allowed.sort(key=lambda x: x[nhids["parkrunner"]].split(" ")[::-1])

        removes = [
            nhids["parkrun"],
            nhids["number"],
            nhids["Gender"],
            nhids["Gender Pos"],
            nhids["Gender Pos Points"],
        ]
        if parkrun != "Wild Card":
            removes.append(nhids["Location"])

        allowed = remove_columns([newheads] + allowed, removes)
        columnaligns = remove_columns([newheadsalign], removes)[0]
        part = BACKHEAD + "<h2>" + parkrun + "</h2>"
        write_html(parkrun, part + create_html_table(allowed, columnalign=columnaligns))

    # Create the per-date files
    for date in dates:
        allowed = list(filter(lambda x: x[nhids["Date"]] == date, maintable))

        random.shuffle(allowed)
        allowed.sort(key=lambda x: x[nhids["Time"]])
        allowed.sort(key=lambda x: x[nhids["Location"]])

        removes = [
            nhids["parkrun"],
            nhids["Date"],
            nhids["number"],
            nhids["Gender"],
            nhids["Cat Pos Points"],
            nhids["Gender Pos Points"],
            nhids["PB Points"],
            nhids["Total Points"],
            nhids["Age Grade Points"],
        ]

        allowed = remove_columns([newheads] + allowed, removes)
        columnaligns = remove_columns([newheadsalign], removes)[0]

        sal = sorted(allowed[1:], key=lambda x: float(x[6].strip("%")))
        highlight = sal[-3:]

        part = f"{BACKHEAD}<h2>{uk_date(date)}</h2><p>{BESTHIGH}</p>"
        write_html(
            date,
            part
            + create_html_table(allowed, columnalign=columnaligns, highlight=highlight),
        )

    # Create the Latest page
    allowed = list(filter(lambda x: x[nhids["Date"]] == date, maintable))

    random.shuffle(allowed)
    allowed.sort(key=lambda x: x[nhids["Time"]])
    allowed.sort(key=lambda x: x[nhids["Location"]])

    removes = [
        nhids["parkrun"],
        nhids["Date"],
        nhids["number"],
        nhids["Gender"],
        nhids["Cat Pos Points"],
        nhids["Gender Pos Points"],
        nhids["PB Points"],
        nhids["Total Points"],
        nhids["Age Grade Points"],
    ]

    # Add the total parkruns column
    allowed = remove_columns(allowed, removes)
    heads = remove_columns([newheads + ["Total Parkruns"]], removes)
    for row in allowed:
        try:
            row.append(totalparkruns[row[1]])
        except KeyError:
            row.append("Unknown")
    sal = sorted(allowed, key=lambda x: float(x[6].strip("%")))
    highlight = sal[-3:]

    # Split into separate tables
    alloweds = {}
    for run in allowed:
        alloweds.setdefault(run[0], []).append(run)

    for key, allowed in alloweds.items():
        alloweds[key] = [x.copy() for x in heads] + allowed
        for row in alloweds[key]:
            row.remove(row[0])

    columnaligns = remove_columns([newheadsalign + ["text-align:center"]], removes)[0][
        1:
    ]

    alloweds = list(alloweds.items())
    random.shuffle(alloweds)
    alloweds = dict(sorted(alloweds, key=lambda x: len(x[1]), reverse=True))

    text = f"{BACKHEAD}<h2>Latest ({uk_date(date)})</h2><p>{BESTHIGH}</p>"
    for key, allowed in alloweds.items():
        text += f"<h3>{key}</h3>"
        text += create_html_table(
            allowed, columnalign=columnaligns, highlight=highlight
        )
    write_html("Latest", text)

    # Create the per-runner files

    # Open the cross-site individual pages
    with open("../RunnerPages/pages.txt", "r") as f:
        pages = eval(f.read())

    for runner in names:
        allowed = list(filter(lambda x: x[nhids["parkrunner"]] == runner, maintable))
        bests = []

        bests += find_highest_only(
            allowed, nhids["parkrun"], nhids["Total Points"], special={"Wild Card": 3}
        )[1]
        random.shuffle(allowed)
        allowed.sort(key=lambda x: x[nhids["Date"]])

        removes = (
            nhids["parkrun"],
            nhids["parkrunner"],
            nhids["number"],
            nhids["Gender"],
            nhids["Gender Pos"],
            nhids["Gender Pos Points"],
        )

        allowed = remove_columns([newheads] + allowed, removes)
        columnaligns = remove_columns([newheadsalign], removes)[0]

        bests = remove_columns(bests, removes)

        # Work out volunteering points
        try:
            volunteered = vpoints[runner]
        except KeyError:
            volunteered = 0
        volunteeredpoints = volunteeringpoints(volunteered)
        tourisms = count_unique(x[0] for x in allowed) - 1
        totalpoints = (
            sum([x[10] for x in bests]) + volunteeredpoints + tourpoints(tourisms)
        )
        firstname = runner.split(" ")[0]
        header = f'{BACKHEAD}<h2>{runner}</h2><table class="minibox" style="width:{MINIWIDTH}px"><th><a href="../Runner Pages/{runner}.html">Click to see results for all of {firstname}\'s competitions.</a></th></table>'

        volunteeredtext = (
            f"<p>Volunteered {volunteered} times ({volunteeredpoints} points)</p>"
        )
        if volunteered == 0:
            volunteeredtext = volunteeredtext.replace(
                "Volunteered 0 times", "Not volunteered"
            )
        if volunteered == 1:
            volunteeredtext = volunteeredtext.replace("1 times", "once")
        elif volunteered == 2:
            volunteeredtext = volunteeredtext.replace("2 times", "twice")

        touredtext = (
            f"<p>Done {tourisms} different parkruns ({tourpoints(tourisms)} points)</p>"
        )
        totaltext = f"<p>{totalpoints} total points.</p>"
        try:
            totalparkrunstext = (
                f"<p>Lifetime total: {totalparkruns[runner]} parkruns</p>"
            )
        except KeyError:
            totalparkrunstext = ""
        part = header + totalparkrunstext + volunteeredtext + touredtext + totaltext
        write_html(
            runner,
            part
            + create_html_table(allowed, highlight=bests, columnalign=columnaligns),
        )

        # Cross-site
        crosssite = f"<em>{totalpoints} points<br>The parkruns which are counting towards this are:</em><br>{create_html_table([allowed[0]] + bests, columnalign=columnaligns, link=False)}<p><em> + {volunteeredpoints} volunteering points</em></p><p><em> + {tourpoints(tourisms)} tourism points</em></p>"
        if runner not in pages:
            pages[runner] = {}
        pages[runner]["parkrun Competition"] = crosssite

    # Cross-site
    with open("../RunnerPages/pages.txt", "w") as f:
        f.write(str(pages))

    # Make the overall winning table
    fulltable = []
    fulltableheads = (
        ["parkrunner", "Gender", "Total Points"]
        + PARKRUNS
        + ["Volunteering", "Tourism"]
    )
    for runner in names:
        try:
            volunteered = vpoints[runner]
        except KeyError:
            volunteered = 0
        runs = []
        for run in maintable:
            if run[nhids["parkrunner"]] == runner:
                runs.append(run)

        # Calculate tourism points
        tourp = tourpoints(count_unique(x[1] for x in runs))

        ones, runs = find_highest_only(
            runs, nhids["parkrun"], nhids["Total Points"], special={"Wild Card": 3}
        )

        # Make the list full
        for parkrun in PARKRUNS:
            if parkrun not in ones:
                fake = runs[0][:]
                fake[nhids["parkrun"]] = parkrun
                fake[nhids["Total Points"]] = ""
                runs.append(fake)

        # Compose wild cards
        newruns = []
        init = None
        for run in runs:
            if run[nhids["parkrun"]] != "Wild Card":
                newruns.append(run)
            elif init is None:
                newruns.append(run)
                init = len(newruns) - 1
            else:
                newruns[init][nhids["Total Points"]] += run[nhids["Total Points"]]
        runs = newruns

        runs.sort(key=lambda x: PARKRUNS.index(x[nhids["parkrun"]]))

        # Calculate volunteering points
        vp = volunteeringpoints(volunteered)
        if vp == 0:
            vp = ""
        bests = [run[nhids["Total Points"]] for run in runs] + [vp, tourp]
        totpoints = 0
        for perf in bests:
            if perf != "":
                totpoints += perf

        theirdata = [runner, runs[0][nhids["Gender"]], totpoints] + bests

        fulltable.append(theirdata)

    # Sort the runner's scores
    random.shuffle(fulltable)
    fulltable.sort(key=lambda x: x[fulltableheads.index("Total Points")], reverse=True)

    # Find those who have qualified
    highlight = []
    for i in fulltable:
        if i[fulltableheads.index("Volunteering")]:
            highlight.append(i)

    # Make and write the table
    table = create_html_table(
        [fulltableheads] + fulltable, columnalign=fulltablealign, highlight=highlight
    )
    write_html("Overall", MFHEAD + "<h2>Overall Standings</h2>" + table)

    # Male and female tables
    genders = {"M": "Male", "F": "Female"}
    for gender in genders:
        filt = list(
            filter(lambda x: x[fulltableheads.index("Gender")] == gender, fulltable)
        )

        table = create_html_table(
            [fulltableheads] + filt, columnalign=fulltablealign, highlight=highlight
        )
        write_html(
            genders[gender],
            MFHEAD + "<h2>" + genders[gender] + " Standings</h2>" + table,
        )

    # Create the volunteering page
    vtable = sorted(
        [
            [r, vpoints[r], volunteeringpoints(vpoints[r])]
            for r in names
            if r in vpoints
        ],
        key=lambda x: x[1],
        reverse=True,
    )
    table = create_html_table(VHEADS + vtable)
    write_html("Volunteering", f"{MFHEAD}<h2>Volunteering Table</h2>{table}")

    # Create the statistics page
    stattext = ""

    # Total number of runners
    stattext += f"<p>Total runners: {len(names)}</p>"

    # Total number of runs
    stattext += f"<p>Total parkruns run: {len(maintable)}</p>"

    # Total distance
    kms = len(maintable) * 5
    stattext += (
        f"<p>Total distance run: {kms} km ({round(kms / 1.609344, 2)} miles)</p>"
    )

    stattext += (
        f"<p>Total volunteers: {len(tuple(filter(lambda x:vpoints[x], vpoints)))}</p>"
    )

    # Total times volunteered
    stattext += f"<p>Total times volunteered: {sum(vpoints.values())}</p>"

    # Find the total time run by Penistone runners (totalsecs is for average)
    totalsecs = 0
    seconds = 0
    minutes = 0
    hours = 0
    days = 0
    for run in maintable:
        time = run[nhids["Time"]].split(":")
        minutes += int(time[0])
        seconds += int(time[1])
        totalsecs += int(time[0]) + int(time[1]) * 60
    minutes += seconds // 60
    seconds %= 60
    hours += minutes // 60
    minutes %= 60
    days += hours // 24
    hours %= 24
    t = f"{days} days, {hours} hours, {minutes} minutes and {seconds} seconds"

    if days == 1:
        t.replace("days", "day")
    if hours == 1:
        t.replace("hours", "hour")
    if minutes == 1:
        t.replace("minutes", "minute")
    if seconds == 1:
        t.replace("seconds", "second")

    stattext += f"<p>Total time: {t}</p>"

    # Average pace
    kph = len(maintable) * 5 / (totalsecs / 3600)
    mph = kph / 1.609344
    mpk = str(int(60 / kph)) + ":" + str(int((3600 / kph) % 60)).zfill(2)
    mpm = str(int(60 / mph)) + ":" + str(int((3600 / mph) % 60)).zfill(2)
    mpp = str(int(60 / kph * 5)) + ":" + str(int((3600 / kph * 5) % 60)).zfill(2)

    stattext += (
        f"<p>Average speed: {round(kph, 2)} km/h ({round(kph / 1.609344, 2)} mph)"
    )
    stattext += f"<p>Average pace: {mpk} per kilometre ({mpm} per mile), or a parkrun time of {mpp}"

    fastestremoves = [
        nhids["parkrun"],
        nhids["number"],
        nhids["Gender Pos Points"],
        nhids["Cat Pos Points"],
        nhids["Age Grade Points"],
        nhids["Total Points"],
        nhids["PB Points"],
    ]

    # Fastests tables
    allfasts = []

    # Fastest parkruns
    fastests = sorted(
        maintable, key=lambda x: (len(x[nhids["Time"]]), x[nhids["Time"]])
    )
    for i in ("F", "M", None):
        if i is not None:
            fasts = remove_columns(
                filter(lambda x: x[nhids["Gender"]] == i, fastests), fastestremoves
            )
        else:
            fasts = remove_columns(
                sorted(fastests, key=lambda x: x[nhids["Age Grade"]], reverse=True),
                fastestremoves,
            )
        usedrunners = set()
        newfasts = []
        for fast in fasts:
            if len(newfasts) >= TOPNUMBER:
                break
            if fast[1] not in usedrunners:
                usedrunners.add(fast[1])
                newfasts.append(fast)
        allfasts.append(newfasts)

    for i, fasts in enumerate(allfasts):
        fasts = remove_columns([newheads], fastestremoves) + fasts
        stattext += [
            "<h3>Fastest female parkrunners</h3>",
            "<h3>Fastest male parkrunners</h3>",
            "<h3>Best age grades</h3>",
        ][i]
        stattext += create_html_table(fasts)

    write_html("Statistics", BACKHEAD + "<h2>Statistics</h2>" + stattext)

    # Output numbers at each parkrun
    with open("number_data.csv", "r") as f:
        numberdata = f.read()
    numberdata = [x.split(";") for x in numberdata.split("\n")]
    ins = set(x[0] for x in numberdata)
    numbers = collections.Counter([row[nhids["Location"]] for row in maintable])
    # Go through the existing ones, and update them
    removes = []
    for parkrun in numberdata:
        if parkrun[0] in numbers:
            parkrun[2] = str(numbers[parkrun[0]])
            iscounter = str(int(parkrun[0] in PARKRUNS))
            if len(parkrun) == 4:
                parkrun[3] = iscounter
            else:
                parkrun.append(iscounter)
        else:
            removes.append(parkrun)

    # Add any new ones
    for parkrun in numbers:
        if parkrun in ins:
            continue
        numberdata.append(
            [parkrun, "0", str(numbers[parkrun]), str(int(parkrun in PARKRUNS))]
        )

    # Remove other parkruns
    for remove in removes:
        numberdata.remove(remove)
    numberdata.sort(key=lambda x: int(x[2]), reverse=True)
    with open("number_data.csv", "w") as f:
        f.write("\n".join([";".join(x) for x in numberdata]))


if __name__ == "__main__":
    main()
    main()
