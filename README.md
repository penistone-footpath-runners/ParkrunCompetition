Running Process
===============

1. Change the dates in `dates.txt` to the required ones.
2. Run `main.py`
4. Change `Runner_Names.py` to fix any errors, if necessary.
5. Run `GenerateHTML.py` again.
6. Update the runner pages.
7. Upload.
